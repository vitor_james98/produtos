
$("#newProd").click(function(){
    $('#exampleModalLongTitle').text('Cadastrar novo produto');
    $('#exampleModalCenter').modal('show');
    $("#div-status").attr('class','col-12 d-none');
    $("#cadProd").attr('class','btn btn-primary d-block');
    $("#updateProd").attr('class','btn btn-primary d-none');
});

$('#cadProd').click(function(){
    var produto = $("#produto").val();
    var valor = $("#valor").val(); 

    if (valor != '' && produto != '') {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: "/newprod",
            data: {
                nome: produto,
                preco: valor,
            },
            success: function(data){
                location.reload();
            },
            error: function(){
                alert('Erro no Ajax !');
            }
        });
    } else {
        alert('Existem campos vazios.');
    }
});

function getinfo(id){
    $('#exampleModalLongTitle').text('Editar produto');
    $('#exampleModalCenter').modal('show');
    $("#div-status").attr('class','col-12 d-block');
    $("#cadProd").attr('class','btn btn-primary d-none');
    $("#updateProd").attr('class','btn btn-primary d-block');
    $("#updateProd").attr('onclick','editProd('+id+')');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: "/infoprod",
        data: {
            id: id
        },
        success: function(data){
            $("#produto").val(data[0]['nome']);
            $("#valor").val(data[0]['preco']);
            $("#status").val(data[0]['status']);
        }
    });
}

function editProd(id){
    var produto = $("#produto").val();
    var valor = $("#valor").val();
    var status = $("#status").val();  

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: "/updateprod",
        data: {
            id: id,
            nome: produto,
            preco: valor,
            status: status
        },
        success: function(data){
            location.reload();
        },
        error: function(){
            alert('Erro no Ajax !');
        }
    });
}

function delprod(id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: "/delprod",
        data: {
            id: id            
        },
        success: function(data){
            location.reload();
        },
        error: function(){
            alert('Erro no Ajax !');
        }
    });
}


$("#valor").maskMoney();
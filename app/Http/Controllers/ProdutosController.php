<?php

namespace App\Http\Controllers;
use App\Produtos;
use Illuminate\Http\Request;

class ProdutosController extends Controller{
    public function app(){
        $produtos = Produtos::where('status','<>','excluido')->get();
        $title = 'Notridan Tecnologia - Produtos';
        return view('app',['title'=>$title,'prods'=>$produtos]);
    }

    public function cadastrarprod(Request $dados){
        $produto = new Produtos();
        $produto->nome = $dados['nome'];
        $produto->preco = $dados['preco'];
        $produto->status = 'ativo';
        $produto->save();
    }

    public function infoprod(Request $dados){
        $produto = Produtos::where('id','=',$dados['id'])->get();
        return $produto;
    }

    public function updateprod(Request $dados){
        $produto = Produtos::find($dados['id']);
        $produto->nome = $dados->nome;
        $produto->preco = $dados->preco;
        $produto->status = $dados->status;
        $produto->save();
    }

    public function delprod(Request $dados){
        $produto = Produtos::find($dados['id']);
        $produto->status = 'excluido';
        $produto->save();
    }
}

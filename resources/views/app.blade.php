<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=h1, initial-scale=1.0">
    <title>{{$title}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>
<body>
@include('components.header')
@include('components.table')
</body>
@include('components.modal')
<script src="https://demos.creative-tim.com/argon-design-system/assets/js/core/jquery.min.js"></script>
<script src="https://demos.creative-tim.com/argon-design-system/assets/js/core/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script src="/js/functions.js"></script>
</html>
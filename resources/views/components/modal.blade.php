<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header py-3">
        <h5 class="modal-title text-dark font-weight-bold" id="exampleModalLongTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-12 px-0">
            <div class="row mb-3">
                <div class="col-6">
                    <label class="text-dark font-weight-bold">Nome do produto:</label>
                    <input type="text" class="form-control" placeholder="Produto" value="" id="produto"/>
                </div>
                <div class="col-6">
                    <label class="text-dark font-weight-bold">Preço do produto:</label>
                    <input type="text" class="form-control" placeholder="Valor" value="" id="valor"/>
                </div>
                <div class="col-12 d-none" id="div-status">
                    <label class="text-dark font-weight-bold">Status do produto:</label>
                    <select id="status" class="form-control">
                      <option value="ativo">Ativo</option>
                      <option value="inativo">Inativo</option>
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer py-3">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="cadProd">Cadastrar</button>
        <button type="button" class="btn btn-primary d-none" id="updateProd">Salvar</button>
      </div>
    </div>
  </div>
</div>
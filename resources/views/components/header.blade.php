<div class="col-12">
    <div class="row">
        <nav class="navbar navbar-expand-lg navbar-dark bg-default shadow w-100">
            <div class="container col-lg-11 px-0">
                <h2 class="text-white">{{$title}}</h2>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar-default">
                    <div class="navbar-collapse-header">
                        <div class="row">
                            <div class="col-8 collapse-brand">
                                <h4 class="text-dark">{{$title}}</h4>
                            </div>
                            <div class="col-4 collapse-close">
                                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item">
                            <a class="btn btn-white w-100" href="javascript:void(0);" id="newProd">
                                <i class="fas fa-plus mr-1"></i> Novo produto
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
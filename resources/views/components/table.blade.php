<div class="col-lg-12 pt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-9 mt-3 px-0">
            <table class="table table-responsive shadow border">
                <thead class="thead-dark">
                    <tr>
                        <th class="text-center" style="width:10%;">ID</th>
                        <th style="width:20%;">Data criação</th>
                        <th style="width:29%;">Nome</th>
                        <th style="width:20%;">Preço</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody id="tBody">
                @foreach($prods as $p)
                    <tr>
                        <td class="text-center">{{$p['id']}}</td>
                        <td>{{date("d/m/Y",strtotime($p['created_at']))}}</td>
                        <td>{{$p['nome']}}</td>
                        <td>{{'R$ '.number_format(($p['preco']), 2, ',', '.')}}</td>
                        <td class="d-flex">
                        <button type="button" rel="tooltip" class="btn btn-info btn-sm" title="Editar" onclick="getinfo({{$p['id']}})">
                            <i class="fa fa-pencil-alt mr-1"></i> Editar
                        </button>
                        <button type="button" class="btn btn-danger btn-sm" title="Excluir" onclick="delprod({{$p['id']}})">
                        <i class="fa fa-trash mr-1"></i> Excluir
                        </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>